import React, { Component } from "react";
import "./static/styles/App.css";
import { GameMenu } from "./components/GameMenu.js";
import { subscribeToServer } from "./api.js";

const GameContext = React.createContext();

class App extends Component {
  constructor() {
    super();

    this.state = {
      players: []
    };

    subscribeToServer(
      (err, data) => {
        console.log(data);
      },
      (err, data) => {
        console.log('new player')
        this.setState({
          players: data
        });
      }
    );
  }
  render() {
    return (
      <div className="App">
        <GameContext.Provider value={this.state.players}>
          <GameMenu />
        </GameContext.Provider>
      </div>
    );
  }
}

export default App;
export { GameContext };
