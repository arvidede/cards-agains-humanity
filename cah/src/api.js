import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:8000');

function subscribeToServer(log, newGame) {
    socket.emit('newClient')
    socket.on('clientJoinedGame', data => newGame(null, data))
}

function startNewGame(id, requestHandler) {
    socket.emit('newGame', id)
    socket.on('initRequest', response => requestHandler(response))
}

function joinGame(id, requestHandler) {
    socket.emit('joinGame', id)
    socket.on('loginRequest', response => requestHandler(response))
}

socket.io.on('connect_error', function(err, event) {
    console.log('Error connecting to server');
});

export { subscribeToServer, startNewGame, joinGame};
