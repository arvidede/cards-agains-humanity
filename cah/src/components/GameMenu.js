import React, { Component } from "react";
import "../static/styles/GameMenu.css";
import { startNewGame, joinGame } from "../api.js";
import { Cards } from "./Cards.js";
import { Game } from "./Game.js";

class GameMenu extends Component {
    constructor() {
        super();
        this.state = {
            inputValue: "",
            isGame: false,
            error: ""
        };
        this.handleNewGame = this.handleNewGame.bind(this);
        this.handleJoinGame = this.handleJoinGame.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.startGame = this.startGame.bind(this);
    }

    handleNewGame(event) {
        event.preventDefault();
        if (this.validateForm()) {
            startNewGame(this.state.inputValue, response =>
                this.handleRequest(
                    response,
                    "Game ID in use, please try another"
                )
            );
        }
    }

    handleJoinGame(event) {
        event.preventDefault();
        if (this.validateForm()) {
            joinGame(this.state.inputValue, response =>
                this.handleRequest(response, "ID not found")
            );
        }
    }

    handleRequest(response, error) {
        if (response) {
            this.startGame();
        } else {
            console.log("Id in use");
            this.setState({
                error: error
            });
        }
    }

    validateForm() {
        return this.state.inputValue !== "";
    }

    startGame() {
        this.setState({
            isGame: true
        });
    }

    handleInputChange(value) {
        this.setState({
            inputValue: value
        });
    }

    render() {
        return (
            <div>
                {this.state.isGame ? (
                    <Game gameId={this.state.inputValue} />
                ) : (
                    <div>
                        <Cards />
                        {this.state.error !== "" && (
                            <p className="InitError">{this.state.error}</p>
                        )}
                        <Form
                            onSubmit={this.handleNewGame}
                            value="Nytt spel"
                            onChange={this.handleInputChange}
                        />
                        <Form
                            onSubmit={this.handleJoinGame}
                            value="Anslut"
                            onChange={this.handleInputChange}
                        />
                    </div>
                )}
            </div>
        );
    }
}

const Form = props => {
    return (
        <form onSubmit={props.onSubmit} className="LoginForm">
            <input
                onChange={event => props.onChange(event.target.value)}
                type="text"
                placeholder="ID"
            />
            <input type="submit" value={props.value} />
        </form>
    );
};

export { GameMenu };
