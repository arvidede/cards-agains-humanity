import React, { Component } from "react";
import "../static/styles/Cards.css";
import { data } from "../static/data/cards.js"
import { quest, ans } from "../static/data/data.js";

class Cards extends Component {
  constructor() {
    super();
    const questions = [];
    const answers = [];
    for (const card of data) {
      if (card.cardType === "A") {
        answers.push(card);
      } else {
        questions.push(card);
      }
    }

    const q = questions.concat(quest);
    const a = answers.concat(ans);

    this.state = {
      questions: q,
      answers: a,
      currAnswer: "",
      currQuestion: ""
    };
  }

  generateNew = () => {
    const randomAns = Math.floor(Math.random() * this.state.answers.length);
    const randomQ = Math.floor(Math.random() * this.state.questions.length);
    let answer = ''
    if(this.state.answers[randomAns].text) {
      answer = this.state.answers[randomAns].text
    } else {
      answer = this.state.answers[randomAns]
    }
    let secondAnswer = ''
    if(this.state.answers[randomAns-1].text) {
      secondAnswer = this.state.answers[randomAns-1].text
    } else {
      secondAnswer = this.state.answers[randomAns-1]
    }

    this.setState({
      currAnswer: answer,
      secondAnswer: secondAnswer,
      currQuestion: this.state.questions[randomQ]
    });
    // console.log(randomAns, this.state.answers[randomAns], this.state.answers[randomAns-1], this.state.questions[randomQ])
  };

  componentDidMount() {
    this.generateNew();
  }

  render() {
    return (
        <div className="Content">
          <div className="TextContent">
            <div className="Question">{this.state.currQuestion.text}</div>
            <div className="Answer">
              {this.state.currAnswer}
              {(this.state.currQuestion.pick === 2 ||
                this.state.currQuestion.numAnswers === 2) && (
                <p>{this.state.secondAnswer}</p>
              )}
            </div>
          </div>
          <button className="NewQuestion" onClick={this.generateNew}>
            Ny
          </button>
        </div>
    );
  }
}

export { Cards };
