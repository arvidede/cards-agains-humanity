import React, { Component } from "react";
import { GameContext } from '../App.js'

class Game extends Component {
    constructor(props) {
        super(props)
        this.state = {
            gameId: props.gameId
        }
    }

    render() {
        return <div>
         New game: {this.state.gameId}
        <GameContext.Consumer>
        {(players) => (
            <p>Players: {players.length}</p>
        )}
        </GameContext.Consumer>
        </div>;
    }
}

export { Game };
