const express = require('express')

/* Client socket */

const io = require('socket.io')();

const clientPort = 8000;
io.listen(clientPort);

const activeGames = {games: []}

/* Socket */

io.on('connection', (client) => {
    console.log('New client!')

    /* New Game, checks if game id already is in use */

    client.on('newGame', (id) => {
        if(!activeGames.games.includes(id)){
            activeGames.games.push(id)
            activeGames[id] = [client.id]
            console.log('New game: ', activeGames)
            client.join(id)
            client.emit('initRequest', true)
        } else {
            console.log('Game-id already in use')
            client.emit('initRequest', false)
        }
    })

    /* New player, checks if game id is valid */

    client.on('joinGame', (id) => {
        if(activeGames.games.includes(id)) {
            console.log('Client', client.id, 'joined game: ', id)
            client.emit('loginRequest', true)
            client.join(id)
            activeGames[id].push(client.id)
            io.in(id).emit('clientJoinedGame', activeGames[id])
            console.log(activeGames[id])
        } else {
            client.emit('loginRequest', false)
        }
    })

});


// sending to sender-client only
// socket.emit('message', "this is a test");

// // sending to all clients, include sender
// io.emit('message', "this is a test");

// // sending to all clients except sender
// socket.broadcast.emit('message', "this is a test");

// // sending to all clients in 'game' room(channel) except sender
// socket.broadcast.to('game').emit('message', 'nice game');

// // sending to all clients in 'game' room(channel), include sender
// io.in('game').emit('message', 'cool game');

// // sending to sender client, only if they are in 'game' room(channel)
// socket.to('game').emit('message', 'enjoy the game');

// // sending to all clients in namespace 'myNamespace', include sender
// io.of('myNamespace').emit('message', 'gg');

// // sending to individual socketid
// socket.broadcast.to(socketid).emit('message', 'for your eyes only');

console.log('Client: Listening on port', clientPort);

/* Server */

const server = express()
const port = 3001
server.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    next()
})
server.listen(port)
